var wumpusGameWidth = 960;
var wumpusGameHeight = 640;
var reffGame = null;

function isRoomMaster(){
	if(roomMasterName == state.userName){
		return true;
	}
	return false;
}

function getTotalGoldInTim(timIdx){
	let gold = 0;
	for(let i=0; i < playerPos[timIdx].length; i++){
		gold += playerPos[timIdx][i].gold;
	}
	return gold;
}

function isGameFinish(){
	let tim = [];
	tim.push(getTotalGoldInTim(0));
	tim.push( getTotalGoldInTim(1));
	if(tim[0] + tim[1] == goldInMap){
		updateEmas(tim[myTeamIdx] , tim[enemyTeamIdx] , ( goldInMap -(tim[0] + tim[1]) ) );
		finishGameWumpus();
		if(tim[0] == tim[1]){
			//$("#status").html("Game Draw : Jumlah emas sama");
			showPopUpMessage( "Game Draw" , "Gold count in every team is same" );
			pushMessageServer('Server' ,  'Gold count in every team is same, game draw');
			
		}else{
			if(tim[myTeamIdx] < tim[enemyTeamIdx]){
				//$("#status").html("You lose Tim mu"+tim[myTeamIdx] +" Tim musuh :" + tim[enemyTeamIdx]);
				showPopUpMessage( "Game Lose" , "Try again in next match, enemy gold is higher than you" );
				pushMessageServer('Server' ,  'Try again in next match, enemy gold is higher than you, game lose');
			}else{
				//$("#status").html("You win Tim mu"+tim[myTeamIdx] +" Tim musuh :" + tim[enemyTeamIdx]);
				showPopUpMessage( "Game win" , "Congratulations you're winner, celebrate it with your friends" );
				pushMessageServer('Server' ,  "Congratulations you're winner, celebrate it with your friends , game lose");
			}
			
		}
		console.log("game finish tim 0:",tim[0],"tim 1:",tim[1]);
	}else{
		console.log("game not finish yet tim 0:",tim[0],"tim 1:",tim[1] , 'all' , goldInMap );
		//pushMessage("Emas mu:"+playerPos[myTeamIdx][myArrayIdx].gold+" Tim mu:"+tim[myTeamIdx]+" Tim Musuh:"+tim[enemyTeamIdx]+" Tersisa di map:"+(goldInMap-(tim[0]+tim[1])) );
		updateEmas(tim[myTeamIdx] , tim[enemyTeamIdx] , ( goldInMap -(tim[0] + tim[1]) ) );
	}
}

function finishGameWumpus(){
	if(reffGame != null){
		reffGame.destroy();
		reffGame = null;
	}
}
function startGameWumpus(){
	var game = new Phaser.Game(
	  wumpusGameWidth,
	  wumpusGameHeight,
	  Phaser.AUTO,
	  'gamePlace','',true
	)

	game.state.add('boot', Game.Boot);
	game.state.add('preload', Game.Preload);
	game.state.add('mainmenu', Game.MainMenu);
	reffGame = game;
	// Load boot scene
	game.state.start('boot');
}

function AddText(x, y, text, fontStyle, fontSize, fill, align){
  return reffGame.add.text(x, y, text, {font: fontStyle + ' ' + fontSize + 'px Century Gothic', fill: fill, align: align});
}


function clamp(val, min, max){
    return Math.max(min, Math.min(max, val));
}

function getImage(name){
  return "assets/img/"+name;
}
function getAudio(name){
  return "assets/audio/"+name;
}

function getJson(name){
  return "assets/data/"+name;
}

function randomInt(min,max){
	return Math.floor(Math.random() * (max - min)) + +min; 
}

function getDistance(pos1 , pos2){
	let x = Math.abs(pos2.x - pos1.x);
	let y = Math.abs(pos2.y - pos1.y);
	return x+y;
}
