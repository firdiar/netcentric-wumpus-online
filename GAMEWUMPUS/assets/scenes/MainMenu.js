Game.MainMenu = function(game){}



// wumpus code 1
// hole code 2
// gold code 3
//[wumpus , breeze , aura , trap]
function move(x , y , xLast , yLast , myTeamIdx , myArrayIdx){
	Game.MainMenu.prototype.movementLeft--;
	//console.log(Game.MainMenu.prototype.movementLeft);
	client.emit('movementUpdate' , state.currentGameRoom,{x:x ,y:y , xLast:xLast , yLast:yLast, teamIdx:myTeamIdx , arrayIdx:myArrayIdx} );
	Game.MainMenu.prototype.reRender(x,y,xLast,yLast, myTeamIdx ,myArrayIdx , true);
}
function shoot(x,y){
	Game.MainMenu.prototype.movementLeft--;
	Game.MainMenu.prototype.shootLeft--;
	let hit = null;
	console.log( "Shoot :",{x:x,y:y} , playerPos[enemyTeamIdx]);
	for(let i=0; i < playerPos[enemyTeamIdx].length; i++){
		console.log("enemy dead "+i,playerPos[enemyTeamIdx][i].dead);
		if(playerPos[enemyTeamIdx][i].dead){continue;}
		if(x == playerPos[enemyTeamIdx][i].x && y == playerPos[enemyTeamIdx][i].y){
			hit = {tim:enemyTeamIdx , idx:i};
			break;
		}
	}
	if(hit == null){
		console.log("serangan meleset");
		showToastMessage("Panah ditembakan serangan meleset" , 3000);
	}else{
		console.log("serangan mengenai target!");
		showToastMessage("Panah ditembakan serangan mengenai target!" , 3000);
		client.emit("playerLifeUpdate" , state.currentGameRoom ,{ tim:hit.tim , idx:hit.idx , dead:true , cause:3});
		playerPos[hit.tim][hit.idx].dead = true;
	}
}

function isAvailableMoveThere(x,y){
	if( (x < 0 || y < 0) || ( x*80 >=wumpusGameWidth || y*80 >= wumpusGameHeight)){
		return false;
	}
	for(let i=0; i < playerPos[myTeamIdx].length; i++){
		if(i==myArrayIdx){
			continue;
		}
		if(x==playerPos[myTeamIdx][i].x && y==playerPos[myTeamIdx][i].y){
			return false;
		}
	}
	return true;
}


/*
Left x -1
Right x +1
Up y -1
Down y +1
*/
function shootTo(x , y){// argument is vector shoot
	if(Game.MainMenu.prototype.movementLeft <= 0){
		console.log("cant move its not your turn");
		showToastMessage("Tidak dapat menembak panah belum giliran-mu" , 3000);
		return;
	}
	if(Game.MainMenu.prototype.shootLeft <= 0){
		showToastMessage("Panah hanya dapat di tembakan 1x tiap turn" , 3000);
		return;
	}
	if(!isAvailableMoveThere(playerPos[myTeamIdx][myArrayIdx].x + x,playerPos[myTeamIdx][myArrayIdx].y + y)){
		console.log("sorry cant shoot to that pos");
		showToastMessage("tidak bisa menembak ke arah itu" , 3000);
		return;
	}
	shoot(playerPos[myTeamIdx][myArrayIdx].x + x,playerPos[myTeamIdx][myArrayIdx].y + y);
}
function moveTo(x , y){// argument is vector move
	if(Game.MainMenu.prototype.movementLeft <= 0){
		console.log("cant move its not your turn");
		showToastMessage("Tidak dapat menggerakan karakter belum giliran-mu" , 3000);
		return;
	}
	if(!isAvailableMoveThere(playerPos[myTeamIdx][myArrayIdx].x + x,playerPos[myTeamIdx][myArrayIdx].y + y)){
		console.log("sorry cant move there");
		showToastMessage("Area terlarang untuk bergerak" , 3000);
		return;
	}
	
	console.log("move up");
	let xLast = playerPos[myTeamIdx][myArrayIdx].x;
	let yLast = playerPos[myTeamIdx][myArrayIdx].y;
	playerPos[myTeamIdx][myArrayIdx].x += x;
	playerPos[myTeamIdx][myArrayIdx].y += y;
	
	move(playerPos[myTeamIdx][myArrayIdx].x , playerPos[myTeamIdx][myArrayIdx].y , xLast , yLast , myTeamIdx , myArrayIdx);
}

Game.MainMenu.prototype = {
	map : [],
	players:[],
	myChar : null,
	create : function(){
		console.log("MainMenu");
		console.log('GameStarted',gameMap , playerPos);
		
		
		key1 = this.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
		key1.onDown.add(()=>{ if(!isTexting){ moveTo(1,0) } }, this);

		key2 = this.input.keyboard.addKey(Phaser.Keyboard.UP);
		key2.onDown.add(()=>{ if(!isTexting){ moveTo(0,-1) } }, this);

		key3 = this.input.keyboard.addKey(Phaser.Keyboard.LEFT);
		key3.onDown.add(()=>{ if(!isTexting){ moveTo(-1,0) } }, this);
		
		key4 = this.input.keyboard.addKey(Phaser.Keyboard.DOWN);
		key4.onDown.add(()=>{ if(!isTexting){ moveTo(0,1) } }, this);
		
		
		showToastMessage("Permainan dimulai dalam 5 detik<br>Hajar mereka!" , 3000);
		this.stage.disableVisibilityChange = true;
		for(let i = 0 ; i < gameMap.length ; i ++){
			this.map.push([]);
			for(let j = 0 ; j < gameMap[i].length ; j ++){
				const bg = this.add.image(i*80+40, j*80+40, 'tiles');
				bg.anchor.setTo(0.5);
				bg.width = 80;
				bg.height = 80;
				bg.frame = 0;
				this.map[i].push(bg);
			}
		}
		
		//matikan loading
		isGameFinish();
		let popUp = document.querySelector(".loading-page-play");
		$("#loadingStatus").html("Permainan dimulai...");
		popUp.classList.toggle("hide-loading-page-play");
		setTimeout(()=>{popUp.style.display = "none";}, 500);
		
		
		let x = 0;
		let y = 0;
		for(let i = 0 ; i < playerPos[myTeamIdx].length; i++){
			x = playerPos[myTeamIdx][i].x 
			y = playerPos[myTeamIdx][i].y;
			this.map[x][y].frame = 2;
			let  p = this.add.image(this.map[x][y].x, this.map[x][y].y, 'character');
			p.anchor.setTo(0.5);
			if( playerPos[myTeamIdx][i].player.user.name != state.userName){
				p.tint = '0x7af25c';
			}
			this.players.push(p);
			
			$('#playerListHolder').append(getElementPlayerList(playerPos[myTeamIdx][i]));
			updateStatusList(playerPos[myTeamIdx][i])
			
		}
		if(isRoomMaster()){
			reffGame.time.events.add(Phaser.Timer.SECOND * 5, ()=>{ 
				client.emit('nextTurn' , state.currentGameRoom); 
			}, this);
		}
	},
	reRender : function(x , y ,xLast , yLast, teamIdx , playeridx , yourMovement){
		
		playerPos[teamIdx][playeridx].x = x;
		playerPos[teamIdx][playeridx].y = y;
			
		if(teamIdx == myTeamIdx){
			if(playerPos[teamIdx][playeridx].dead){
				this.players[playeridx].alpha = 0.4;
			}else{
				this.players[playeridx].alpha = 1;
			}
			
			
			updateStatusList(playerPos[teamIdx][playeridx]);
			//jika tim kita , maka re render
			this.map[xLast][yLast].frame = 0;
			switch(gameMap[x][y]){
				case 0:
					this.map[x][y].frame = 2;
				break;
				case 1:
					this.map[x][y].frame = 2;
					let w = reffGame.add.image(x*80+40, y*80+40, 'wumpus');
					w.anchor.setTo(0.5);
				break;
				case 2:
					this.map[x][y].frame = 3;
				break;
				case 3:
					this.map[x][y].frame = 1;
				break;
			}

			this.players[playeridx].x = x*80+40;
			this.players[playeridx].y = y*80+40;
		}
		
		// check movement
		if(yourMovement){
			let cond = this.getCondition(x,y);
			console.log('my move condition');
			console.log(this.conditionToString(cond));
			//pushMessage(this.conditionToString(cond));
			pushMessageServer('Server' ,  'Update status kondisi saat ini :\n'+this.conditionToString(cond));
		
			if(cond[2] != 0){
				showToastMessage("Hati-Hati Aura membunuh terdeteksi" , 3000);
			}
			if(cond[4] != 0){
				//showToastMessage("Kamu terbunuh oleh musuh" , 3000);
				this.dead("Kamu terbunuh oleh musuh",3);
				
			}
			if(cond[3] != 0 &&  cond[3] != 3){
				switch(cond[3]){
					case 1:
						this.dead("Kamu terbunuh oleh wumpus",1);
						break;
					case 2:
						this.dead("Kamu terbunuh karena jatuh kedalam lubang",2);
						break;
				}
				
			}else if(cond[3] == 3){
				showToastMessage("Kamu menemukan gold!" , 3000);
				this.getGold(x , y);
			}
			this.lastCondition = cond;
		}else{
			let cond = this.getCondition( playerPos[myTeamIdx][myArrayIdx].x , playerPos[myTeamIdx][myArrayIdx].y);
			let isEq = true;
			for(let i = 0 ; i < cond.length; i++){
				if(cond[i] != this.lastCondition[i]){
					isEq = false;
					break;
				}
			}
			if(!isEq){
				console.log('en move condition');
				console.log(this.conditionToString(cond));
				//pushMessage(this.conditionToString(cond));
				pushMessageServer('Server' ,  'Update status kondisi saat ini :\n'+this.conditionToString(cond));
				updateStatusList(playerPos[myTeamIdx][myArrayIdx]);
				if(cond[2] != 0){
					showToastMessage("Hati-Hati Aura membunuh terdeteksi" , 3000 , 1);
				}
				if(cond[4] != 0){
					showToastMessage("Kamu berhasil membunuh musuh" , 3000 , 1);
					this.killGold(cond[5].tim , cond[5].idx);
				}
				this.lastCondition = cond;
			}
		}
		
	},
	isDead : false,
	movementLeft : 0,
	shootLeft : 0,
	playTime : 0,
	eventLoop : null,
	lastCondition : [],
	turn : function(){
		if(this.isDead){
			this.reSpawn();
			client.emit('nextTurn' , state.currentGameRoom); 
			console.log("Your turn is skipped because youre dead");
			this.isDead = false;
			return;
		}
		let cond = this.getCondition(playerPos[myTeamIdx][myArrayIdx].x ,playerPos[myTeamIdx][myArrayIdx].y );
		console.log(this.conditionToString(cond));
		pushMessageServer('Server' ,  'Update status kondisi saat ini :\n'+this.conditionToString(cond));
		
		showToastMessage("Sekarang giliran bermain kamu" , 1500);
		this.playTime = 12;
		this.movementLeft = 8;
		this.shootLeft = 1;
		this.eventLoop = reffGame.time.events.loop(1000,()=>{
			this.playTime--;
			//console.log( "time left", this.playTime);
			if(this.playTime == 0 || this.movementLeft == 0){
				this.playTime = -1;
				this.movementLeft = -1;
				this.shootLeft = 0;
				this.eventLoop.loop = false;
				client.emit('nextTurn' , state.currentGameRoom); 
			}
		});
		
	},
	getGold : function(x , y){//when get gold from the map
		gameMap[x][y] = 0;
		playerPos[myTeamIdx][myArrayIdx].gold++;
		let target = [{x : x , y:y , val : 0}];
		let target2 = [{tim : myTeamIdx , idx:myArrayIdx , val : playerPos[myTeamIdx][myArrayIdx].gold}];
		client.emit("goldPosUpdate" , state.currentGameRoom , {map:target , player:target2});
		isGameFinish();
	},
	killGold:function(tim , idx){
		if(playerPos[tim][idx].gold ==0){
			console.log('enemyHasNoGold');
			return;
		}
		playerPos[myTeamIdx][myArrayIdx].gold += playerPos[tim][idx].gold;
		playerPos[tim][idx].gold = 0;
		let target2 = [{tim : myTeamIdx , idx:myArrayIdx , val : playerPos[myTeamIdx][myArrayIdx].gold},{tim:tim , idx:idx , val:0} ];
		client.emit("goldPosUpdate" , state.currentGameRoom , {map:[] , player:target2});
		isGameFinish();
	},
	spreadGold : function(count){
		let targetPos = [];
		for(let i=0; i < count; i++){
			targetPos.push(this.getEmptyPosition());
			targetPos[i].val = 3;
		}
		return targetPos;
	},
	dead : function(msg , cause){
		//cause 1 : wumpus , 2 : hole , 3 : player
		this.isDead = true;
		if(msg != null || msg != ""){
			showToastMessage(msg , 3000);
		}
		if(cause != 3 && playerPos[myTeamIdx][myArrayIdx].gold!=0){//spread gold when you die because of obstacle
			let locGold = this.spreadGold(playerPos[myTeamIdx][myArrayIdx].gold);
			playerPos[myTeamIdx][myArrayIdx].gold = 0;
			let target2 = [{tim : myTeamIdx , idx:myArrayIdx , val : 0}];
			client.emit("goldPosUpdate" , state.currentGameRoom , {map:locGold , player:target2});
			for(let i=0; i < locGold.length; i++){
				gameMap[locGold[i].x][locGold[i].y] = locGold[i].val;
				console.log("emas :",locGold[i].x ,locGold[i].y);
			}
		}
		
		this.movementLeft = 0;
		playerPos[myTeamIdx][myArrayIdx].dead = true;
		this.players[myArrayIdx].alpha = 0.4;
		client.emit("playerLifeUpdate" , state.currentGameRoom ,{ tim:myTeamIdx , idx:myArrayIdx , dead:true , cause:cause});
		isGameFinish();
		updateStatusList(playerPos[myTeamIdx][myArrayIdx]);
	},
	reSpawn:function(){
		
		this.movementLeft++;
		playerPos[myTeamIdx][myArrayIdx].dead = false;
		this.players[myArrayIdx].alpha = 1;
		client.emit("playerLifeUpdate" , state.currentGameRoom ,{ tim:myTeamIdx , idx:myArrayIdx , dead:false});
		let pos = this.getEmptyPosition();
		let last ={ x: playerPos[myTeamIdx][myArrayIdx].x , y: playerPos[myTeamIdx][myArrayIdx].y }
		move(pos.x , pos.y , last.x , last.y , myTeamIdx , myArrayIdx);
	},
	getEmptyPosition : function(){
		let isFind = false;
		let x = 0;
		let y = 0;
		while(!isFind){
			x = Math.floor(Math.random()*(wumpusGameWidth/80));
			y = Math.floor(Math.random()*(wumpusGameHeight/80));
			if(gameMap[x][y] == 0){
				isFind = true;
				for(let i=0; i < playerPos.length; i++){
					for(let j=0; j < playerPos[i].length; j++){
						if(playerPos[i][j].dead){
							continue;
						}
						if(playerPos[i][j].x == x && playerPos[i][j].y == y){
							isFind = false;
						}
					}
					if(!isFind){
						break;
					}
				}
			}
		}
		return {x:x , y:y};
	},
	getCondition : function(x , y){
		//wumpus 0 , lubang 1 , aura membunuh 2,terkena trap/gold 3 , bertemu player 4
		if(this.isDead){
			return [0,0,0,0,0];
		}
		result = [0,0,0,0,0];
		if(gameMap[x][y] != 0){
			result[3] = gameMap[x][y];
		}
		for(let i = x-1 ; i <= x+1 ; i++){
			if(i==x || i==-1 || i>=gameMap.length){continue;}
			if(gameMap[i][y] != null){
				switch(gameMap[i][y]){
					case 1:
						//wumpus
						result[0] = 1;
						break;
					case 2:
						//hole
						result[1] = 1;
						break;
				}
			}
		}
		for(let i = y-1 ; i <= y+1 ; i++){
			if(i==y || i==-1 || i>=gameMap[x].length){continue;}
			if(gameMap[x][i] != null){
				switch(gameMap[x][i]){
					case 1:
						//wumpus
						result[0] = 1;
						break;
					case 2:
						//hole
						result[1] = 1;
						break;
				}
			}
		}
		for(let i =0; i < playerPos[enemyTeamIdx].length; i++){
			if(playerPos[enemyTeamIdx][i].dead){
				continue;
			}
			let dist = getDistance( {x:playerPos[enemyTeamIdx][i].x , y:playerPos[enemyTeamIdx][i].y} , {x:x , y:y});
			//console.log("distance to enemy :",dist);
			if(dist == 1){
				result[2] = 1;
				break;
			}else if(dist == 0){
				result[4] = 1;
				result[5] = {tim : enemyTeamIdx , idx : i};
			}
		}
		
		return result;
	},
	conditionToString:function(cond){
		if(this.isDead){
			return "Tidak bisa mengecek kondisi ketika mati";
		}
		res = [];
		if(cond[3] != 0){
			let got = "";
			switch(cond[3]){
				case 1:
					got = "bertemu Wumpus";
					break;
				case 2:
					got = "terjebak Lubang";
					break;
				case 3:
					got = "mendapat Emas";
					break;
				
			}
			res.push("Kamu "+got);
		}
		if(cond[0] != 0){
			res.push("Tercium bau busuk");
		}
		if(cond[1] != 0){
			res.push("Hawa terasa dingin kedinginan");
		}
		if(cond[2] != 0){
			res.push("Aura membunuh terdeteksi");
		}
		if(res.length == 0){
			res.push("Area ini aman tidak terdeteksi apapun");
		}
		return res.join('\n');
		
		
	}

}









