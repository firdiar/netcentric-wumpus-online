Game.Preload = function(game){
    this.game = game // keep reference to main game object
}
//global variable
roomMasterName = "";
playerList = null;

//position in playerPos
myTeamIdx = 0;
enemyTeamIdx = 0;
myArrayIdx = 0;
goldInMap = 2;//playerList[0].length*2


gameMap = null;
playerPos = null;

Game.Preload.prototype = {
	loadingText : null,
    preload:function(){
		console.log("Preload");
		this.loadingText = AddText(wumpusGameWidth/2 , wumpusGameHeight/2 , "Loading 0%" , 'bold' , '80'  )
		this.loadingProgress();
		this.load.spritesheet('character' , getImage('CharacterWumpus.png') , 80,80);
		this.load.spritesheet('wumpus' , getImage('Wumpus.png') , 80,80);
		this.load.spritesheet('tiles' , getImage('Tile.png') , 80,80);
    },
	loadingProgress:function(){
		
		this.load.onFileComplete.add(function(progress){ 
			this.loadingText.text = 'Loading ' + progress + '%';
			if(progress==100){
				//console.log(state);
				client.emit('mapLoaded', state.currentGameRoom);
				
			}
		}, this);

	},
    create:function(){
		console.log("preload")
		this.stage.disableVisibilityChange = true;
		if(isRoomMaster()){
			this.generateMap();
		}
		
    },
	generateMap(){
		//neutral code 0
		wumpusCount = 4; // wumpus code 1
		holeCount = 5; // hole code 2
		goldCount = 2 ; // gold code 3
		map = [];
		
		playerCount = 0;
		playerMap = [];
		for(let i = 0 ; i < wumpusGameWidth; i += 80){
			map.push([]);
			for(let j = 0 ; j < wumpusGameHeight ; j += 80){
				map[i/80].push(0);
			}
		}
		//
		while(holeCount != 0 || wumpusCount != 0 || goldCount != 0 ){
			x = Math.floor(Math.random()*(wumpusGameWidth/80));
			y = Math.floor(Math.random()*(wumpusGameHeight/80));
			if(map[x][y] == 0){
				if(holeCount != 0){
					map[x][y] = 2;
					holeCount--;
				}else if(wumpusCount != 0){
					map[x][y] = 1;
					wumpusCount--;
				}else if(goldCount != 0){
					map[x][y] = 3;
					console.log("emas :",x ,y);
					goldCount--;
				}
			}
		}

		playerCount = playerList[0].length;
		playerMap.push([]);
		while(playerCount != 0){
			x = Math.floor(Math.random()*(wumpusGameWidth/80));
			y = Math.floor(Math.random()*(wumpusGameHeight/80));
			if(map[x][y] == 0){
				playerCount--;
				map[x][y] = -1;
				playerMap[0].push({x:x , y:y , player:playerList[0][playerCount], gold:0,dead:false});
			}
		}
		playerCount = playerList[1].length;
		playerMap.push([]);
		while(playerCount != 0){
			x = Math.floor(Math.random()*(wumpusGameWidth/80));
			y = Math.floor(Math.random()*(wumpusGameHeight/80));
			if(map[x][y] == 0){
				playerCount--;
				map[x][y] = -1;
				playerMap[1].push({x:x , y:y , player:playerList[1][playerCount] , gold:0,dead:false});
			}
		}
		

		for(let i=0; i < playerMap[0].length; i++){
			map[playerMap[0][i].x][playerMap[0][i].y] = 0;
		}
		for(let i=0; i < playerMap[1].length; i++){
			map[playerMap[1][i].x][playerMap[1][i].y] = 0;
		}
		console.log("send map");	
		client.emit('gameMap', state.currentGameRoom ,{map:map , playerMap:playerMap});
		
	}
}
